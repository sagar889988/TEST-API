package examples.users;
import com.intuit.karate.cucumber.CucumberRunner;
import com.intuit.karate.cucumber.KarateStats;
import cucumber.api.CucumberOptions;
import net.masterthought.cucumber.Configuration;
import net.masterthought.cucumber.ReportBuilder;
import org.apache.commons.io.FileUtils;
import org.testng.annotations.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import static org.testng.AssertJUnit.assertTrue;

@CucumberOptions(plugin = { "html:target/cucumber-html-report", "json:target/cucumber.json" }, 
                 features = {"src/test/java/examples/users/users.feature" })

public class TestParallel {
        @Test
	public void testParallel() {
                String karateOutputPath = "target";
                KarateStats stats = CucumberRunner.parallel(getClass(), 1, karateOutputPath);
                generateReport(karateOutputPath);
                assertTrue("there are scenario failures", stats.getFailCount() == 0);
        }

        private static void generateReport(String karateOutputPath) {
                Collection<File> jsonFiles = FileUtils.listFiles(new File(karateOutputPath), new String[] { "json" }, true);
                List<String> jsonPaths = new ArrayList<String>(jsonFiles.size());
                for (File file : jsonFiles) {
                        System.out.println(file.getAbsolutePath());
                        jsonPaths.add(file.getAbsolutePath());
                }
                Configuration config = new Configuration(new File("target"), "corWest");
                config.addClassifications("Environment", System.getProperty("karate.env"));
                ReportBuilder reportBuilder = new ReportBuilder(jsonPaths, config);
                reportBuilder.generateReports();
        }

}
